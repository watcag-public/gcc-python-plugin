from collections import OrderedDict
import warnings
import gcc
from halp.directed_hypergraph import DirectedHypergraph

#try:
#    from halp.directed_hypergraph import DirectedHypergraph
#except:
#    print( "pip3 install halp" )


def operatorToStr(self, label, data_type):
    ret = ""
    if label == gcc.MultExpr:
        ret = "*"
    elif label == gcc.PlusExpr:
        ret = "+"
    elif label == gcc.MinusExpr:
        ret = "+"
    elif label == gcc.RdivExpr:
        ret = "/"
    elif label == gcc.BitAndExpr:
        ret = '&'
    elif label == gcc.BitNotExpr:
        ret = '~'
    elif label == gcc.BitIorExpr:
        ret = '|'
    elif label == gcc.BitXorExpr:
        ret = '^'
    elif label == gcc.LshiftExpr:
        ret = '<<'
    elif label == gcc.RshiftExpr:
        ret = '>>'
    elif label == gcc.SsaName:
        ret = '' # seems like this is just a pointer assignment
    else:
        warnings.warn("Unhandled operator"+str(label))
        ret = "."

    if(data_type==None):
        return ret

    if data_type.name != None:
        data_type_name = data_type.name.name
    else:
        return ret

    if data_type_name == "int":
        ret = ret # do nothing
    elif data_type_name == "long unsigned int":
        ret = ret # do nothing
    elif data_type_name == "double":
        ret = ret + "d"
    elif data_type_name == "float":
        ret = ret + "f"
    else:
        ret = ret
    return ret

def to_idfhg(self): # dfg to inverted dataflow hypergraph


    if hasattr(self, 'name'):
        name = self.name
    else:
        name = 'G'

    HG = DirectedHypergraph()

    node_id_map = OrderedDict() # gcc to node id
    labels = OrderedDict() # want to keep track of labels for techmapping
    labels = OrderedDict()
    hedges = OrderedDict()      # need to collect hyperedges

    current_max_vertex_id = 0
    for block in self.cfg.basic_blocks:
        # skip the empty basic blocks
        if not block.gimple:
            continue

        list_of_all_endpoints=[]
        # Support three data widths
        # int, float, double
        for stmt in block.gimple:
            list_of_endpoints=[]
            stmt.walk_tree(self.builddfg, list_of_endpoints, stmt.loc)

            if isinstance(stmt, gcc.GimpleAssign):
                newlhs=str(stmt.lhs).replace("*", "")
                if newlhs in list_of_endpoints:
                    list_of_endpoints.remove(newlhs)
                    list_of_all_endpoints.extend(list_of_endpoints)

                sink_str = stmt.lhs.__str__()


                if sink_str not in node_id_map:
                    node_id_map[sink_str] = str( current_max_vertex_id )
                    current_max_vertex_id = current_max_vertex_id  + 1

                if type( stmt.lhs ) == gcc.MemRef:
                    labels[ node_id_map[sink_str] ] = stmt.lhs.operand.name
                elif type( stmt.lhs ) == gcc.ParmDecl:
                    labels[ node_id_map[sink_str] ] = stmt.lhs.name
                elif type( stmt.lhs ) == gcc.VarDecl:
                    labels[ node_id_map[sink_str] ] = self.operatorToStr(self, stmt.exprcode, stmt.rhs[0].type )
                elif type( stmt.lhs) == gcc.SsaName :
                    labels[ node_id_map[sink_str] ] = self.operatorToStr(self, stmt.exprcode, stmt.rhs[0].type )
                else:
                    warnings.warn("Unsupported feature")
                    #warnings.warn(f"Unsupported feature on line {stmt.loc}")

                for source in stmt.rhs:
                    source_str = source.__str__()


                    if source_str not in node_id_map:
                        node_id_map[source_str] = current_max_vertex_id
                        current_max_vertex_id = current_max_vertex_id  + 1

                    assert( source_str != None )
                    assert( stmt.lhs.__str__() != None )

                    if( type( source ) == gcc.RealCst or type(source) == gcc.IntegerCst ):
                        labels[str( node_id_map[source_str]) ] = source.constant
                    elif( type( source ) == gcc.ParmDecl ):
                        labels[ str( node_id_map[source_str]) ] = source.name

                    source_id = str( node_id_map[source_str] )
                    if source_id in hedges:
                        hedges[ source_id ].append(str(node_id_map[sink_str]))
                    else:
                        hedges[ source_id ] = [ str( node_id_map[sink_str] ) ]


            elif isinstance(stmt, gcc.GimpleCall):
                # fn call (e.g. sqrt() )
                warnings.warn("Calling external functions is not currently supported by Mocarabe")
                #warnings.warn(f"Calling external functions is not currently supported by Mocarabe (line {stmt.loc}")

                newlhs=str(stmt.lhs).replace("*", "")
                if newlhs in list_of_endpoints:
                    list_of_endpoints.remove(newlhs)
                    list_of_all_endpoints.extend(list_of_endpoints)

                sink_str = stmt.lhs.__str__()
                if sink_str not in labels:
                    warnings.warn("No support for assigning to the same lvalue twice on line")
                    #warnings.warn(f"No support for assigning to the same lvalue twice on line {stmt.loc}")

                if sink_str not in node_id_map:
                    node_id_map[sink_str] = str( current_max_vertex_id )
                    current_max_vertex_id = current_max_vertex_id  + 1

                labels[ node_id_map[sink_str] ] = stmt.fndecl.__str__()

                for source in stmt.rhs:
                    source_str = source.__str__()
                    if source_str not in node_id_map:
                        node_id_map[source_str] = current_max_vertex_id
                        current_max_vertex_id = current_max_vertex_id  + 1

                    assert( source_str != None )

                    if( type( source ) == gcc.RealCst or type( source ) == gcc.IntegerCst ):
                        labels[str( node_id_map[source_str]) ] = source.constant
                    elif( type( source ) == gcc.ParmDecl ):
                        labels[ str( node_id_map[source_str]) ] = source.name

                    source_id = str( node_id_map[source_str] )
                    sink_id = str( node_id_map[ source_str ] )
                    if source_id in hedges:
                        hedges[ source_id ].append(str(node_id_map[sink_str]))
                    else:
                        hedges[ source_id ] = [ str( node_id_map[sink_str] ) ]

            elif isinstance( stmt, gcc.GimpleReturn ):
                continue
            else:
                warnings.warn("You may be compiling features unsupported by Mocarabe")
                #warnings.warn(f"You may be compiling features unsupported by Mocarabe on line {stmt.loc}")

        for source, sinks in hedges.items():
            label = labels[source] if source in labels else ""

            HG.add_node( source,
                label=label
            )

            for sink in sinks:
                label = labels[sink] if sink in labels else ""
                HG.add_node( sink,
                    label=label
                )

            HG.add_hyperedge( [source],
                sinks
            )

    return HG
