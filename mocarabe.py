import sys; sys.path.append("src")
sys.path.append("../cgra-ilp/src")
# Output the inverted DFG of a program
import gcc
from gccutils import get_src_for_loc, cfg_to_idfg, invoke_dot

from dataflow_hypergraph import DataflowHypergraph
import json
import shutil


class OutputDFG(gcc.GimplePass):
    def execute(self, func):
        if func and func.cfg:
            name = func.decl.name
            H = cfg_to_idfg( func.cfg, func.decl.name )


            dataflow_hypergraph = DataflowHypergraph()

            hgr_dir = 'hgr/'
            if not os.path.exists( hgr_dir ):
                print( "Creating 'hgr' directory")
                os.makedirs( hgr_dir )
            func_dir = hgr_dir + name + '/'
            if os.path.exists( hgr_dir + name ) and len( hgr_dir + name ) > 2 : #trying to be safe
                print( 'Deleting existing {} directory'.format( func_dir ) )
                shutil.rmtree( func_dir )

            print( 'Creating {} directory'.format( func_dir ) )
            os.makedirs( func_dir )

            df_hypergraph_path = func_dir + name + ".hgr"
            dataflow_hypergraph.serialize_df_hypergraph( H, df_hypergraph_path )
            print(f"Serializing dataflow hypergraph to {df_hypergraph_path}")
ps = OutputDFG(name='show-gimple')
ps.register_after('cfg')
