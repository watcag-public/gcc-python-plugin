# LeetcodeSolutions
Repo to store solutions for various leetcode problems


gcc-python
==========

**Installing** 

~~Valid for Ubuntu 20.04.2 LTS as of May 10th 2021. You will need  python version 3.5 and gcc version 8. This may not work with other versions of python or gcc.~~

Valid for Ubuntu 16.04.2 LTS as of Sep 2nd 2023 -- you'll need to add a PPA `sudo add-apt-repository ppa:jonathonf/gcc`.

Valid for Ubuntu 20.04 LTS as of Sep 6th 2023 for GCC versions {8,9,10} and Python versions 3.{8,9,10,11}

Install GCC and it's required dependencies with the following, where GCC_VERSION denotes the version of GCC you wish to install
```
sudo apt-get install gcc-$GCC_VERSION g++-$GCC_VERSION gcc-$GCC_VERSION-plugin-dev graphviz python-pip
sudo pip install pygments lxml
```

You will need to install the development versions of Python which can be done using the following commands, where PYTHON_VERSION denotes the minor version of Python you wish to use.

```
sudo apt install python3.$PYTHON_VERSION-distutils
sudo apt install python3.$PYTHON_VERSION-dev
```


If the installation is failing, you may need to also add the following PPA with
```
sudo add-apt-repository ppa:deadsnakes/ppa
```

To compile, just use the following, note that this will automatically run the included unit tests.
```
make PYTHON=python3.$PYTHON_VERSION PYTHON_CONFIG=python3.$PYTHON_VERSION-config
```

If you see errors regarding a missing dependency of "halp", run the following to resolve it
```
python3.$PYTHON_VERSION -m pip install halp
```

The default gcc version that will be used will be the default on the system which can be found by running the following:

```
cc -v
```

This can be expliciltly set in the Makefile by setting 

CC=gcc-$GCC_VERSION

**Running**
For executing the code, you have to setup the environment first

```
export LD_LIBRARY_PATH=./gcc-c-api:./
./gcc-with-python examples/show-gimple.py <file.c>
```

This will produce an integer linear program in lp_solve format. Install that solver as follows:

```
sudo apt-get install lp-solve
```

Each C file is currently taken from the `bitgpu/bench` repository as it is nicely packaged up into simple dataflow functions.

The plugin is Free Software, licensed under the GPLv3 (or later).

